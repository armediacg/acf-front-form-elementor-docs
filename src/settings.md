# Settings

In the **Settings** menu of Wordpress click on **ACF Front Form**

![](img/acf_ff_settings.jpg)

## acf_form_head()
This function is used to process, validate and save the submitted form data created by the `acf_form()` function. It will also enqueue all ACF related scripts and styles for the acf form to render correctly. [Read the documentation](https://www.advancedcustomfields.com/resources/acf_form_head/)

Disable this function if you have added `acf_form_head()` manually or using another plugin

## acf_enqueue_uploader()

This will create a hidden WYSIWYG field and enqueue the required JS templates for the WP media popups

Disable this function if you have added `acf_enqueue_uploader()` manually or using another plugin

## Inline JS

This will allow ACF to initialize the fields within the newly added HTML. [Read the documentation](https://www.advancedcustomfields.com/resources/acf_form/#ajax)

Disable this function if you have added the inline JS manually or using another plugin