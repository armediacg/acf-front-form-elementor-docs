## Requirements

- **Advanced Custom Fields**

ACF Front Form uses functions of **Advanced Custum Fields** version **5.6.7**
You can get the fee version on [GitHub](https://github.com/AdvancedCustomFields/acf/releases)

## Nice to have

- **Elementor Page Builder**

Since shortcodes are processed by WordPress, ACF Fron Form Elementor works without Elementor
For a better experience, it's recomanded to have Elementor installed

## Installation

### Download
You can get the latest version of ACF Front Form Elementor from :
1. CodeCanyon at this [Link](https://codecanyon.net/item/acf-front-form-for-elementor-page-builder/21887968)

### Using Wordpress Plugin Manager

1. On the **Plugin** menu click on **Add New**
2. Then click on **Upload Plugin** then **Chose a file** to upload
3. Click on **Install Now**
4. After the plugin is installed, click on **Activate**

### Using FTP

1. Download [ACF Front Form Elementor](https://www.codester.com/items/10074/acf-front-form-for-elementor?ref=armediacg) Plugin to your desktop.
2. Extract the Plugin folder to your desktop.
3. With your FTP program, upload the Plugin folder to the `wp-content/plugins` folder in your WordPress directory online.
4. Go to Plugins screen and find **ACF Front Form Elementor** Plugin in the list.
5. Click Activate to activate it.
