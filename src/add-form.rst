.. _adding-form:
Adding a Form
=============

.. warning:: Before creating any form, make sure you have used **Advanced Custom Fields** to create fields

.. _using-elementor:
Using Elementor
---------------
Use Elementor or manually add shortcode :code:`[acf_front_form]` where you want your form to be displayed

.. image:: img/el.jpg

The ACF Front Form Settings are then displayed (or select the ACF Front Form Element in your page)

.. image:: img/post-att.jpg

For more information about Elementor settings, see attributes_.

.. _using-shortcode:
Using Shortcode
---------------
To add a form in a page or a post, add the fellowing shortcode where you want the form be rendered

.. code:: php
   
   [acf_front_form]

Examples
^^^^^^^^

* Creating and publishing a new post

.. code:: php
   
   [acf_front_form post_id="new_post" np_title="Yay" np_type="post" np_status"=publish"]

Or

.. code:: php
   
   [acf_front_form post_id="new_post" new_post="post_title=Yay post_type=post post_status=publish"]

* Load title, content and all fields of a specific post into a form

.. code:: php
   
   [acf_front_form post_id="31" post_title="Yes" post_content="Yes"]

* Create a form using a specific Field Groups

.. code:: php
   
   [acf_front_form field_groups="group_5a6ce38c5d036"]

Or

.. code:: php

   [acf_front_form field_groups="949"]


.. _attributes:

Attributes
==========

**ACF Front Form Elementor** creates :code:`[acf_front_form]` shortcode whith attributes. this shortcode call the :code:`acf_form()` function which renders the form, most of this attributes corresponds to a parameter of the :code:`acf_form()` function, for more information about :code:`acf_form()` function, `read this documentation <https://www.advancedcustomfields.com/resources/acf_form/>`_

.. note:: The attributes explained here are used by **ACF Front Form Elementor** shortcode

Post Settings
-------------

Defines the :code:`$post` values, they are all mapped to :code:`acf_form()` function. You can find `here <https://codex.wordpress.org/Class_Reference/WP_Post>`_ all variables of the :code:`WP_Post` class

.. image:: img/post-att.jpg

Shortcode attributes
^^^^^^^^^^^^^^^^^^^^
============= =============== =================== ==========================================================================================================================
 Attribute     Type/Values     Default             Description
============= =============== =================== ==========================================================================================================================
 `post_id`     `int, string`   `Current post ID`   The post ID to load data from and save data to. Can also be set to `"new_post"` to create a new post on submit
 `np_title`    `string`                            The title of the new post
 `np_status`   `string`                            The status of the new post
 `np_type`     `string`                            The post type of the new post
 `new_post`    `array`                             A list of post data separated with commas used to create a post. See `wp_insert_post <https://codex.wordpress.org/Plugin_API/Action_Reference/wp_insert_post>`_ for available parameters. The above `post_id` setting must contain a value of `"new_post"`
============= =============== =================== ==========================================================================================================================

.. _form-settings:
Form Settings
-------------

Defines the settings of the rendered form, they are all mapped to :code:`acf_form()` function

.. image:: img/form-att.jpg

Shortcode attributes
^^^^^^^^^^^^^^^^^^^^
====================  =============  ================  =======================================================================================================================================
 Attribute             Type/Values    Default           Description                              
====================  =============  ================  =======================================================================================================================================
 `form`                `checkbox`    `Yes`              Whether or not to create a form element. Useful when adding to an existing form  
 `id`                  `string`      `acf-form`         The id of the `form` element              
 `form_attributes`     `string`                         A list of HTML attributes for the form element  
 `return`              `string`      `current url`      The URL to be redirected to after the form is submit, more details `here <https://www.advancedcustomfields.com/resources/acf_form/>`_
 `submit_value`        `string`      `Update`           The text displayed on the submit button   
 `updated_message`     `string`      `Post updated`     A message displayed above the form after being redirected. Empty for no message  
 `kses`                `checkbox`    `Yes`              Whether or not to sanitize all `$_POST` data with the `wp_kses_post()` function. Defaults to true
====================  =============  ================  =======================================================================================================================================

.. _field-settings:
Fields Settings
---------------

Defines settings related to the fields, they are all mapped to :code:`acf_form()` function

.. image:: img/fields-att.jpg

Shortcode attributes
^^^^^^^^^^^^^^^^^^^^
========================== =========================== ========= ===============================================================================================
  Attribute                        Type/Values          Default   Description                               
========================== =========================== ========= ===============================================================================================
  `post_title`                     `checkbox`                     Whether or not to show the post title text field  
  `post_content`                   `checkbox`                     Whether or not to show the post content editor field  
  `field_groups`                     `array`                      A list of group IDs/keys separated with commas to override the fields displayed in this form  
  `fields`                           `array`                      A list of field IDs/keys separated with commas to override the fields displayed in this form  
  `label_placement`                `top, left`          `top`     Determines where field labels are places in relation to fields  
  `instruction_placement`        `label, field`         `label`   Determines where field instructions are places in relation to fields  
  `field_el`                `div, tr, td, ul, ol, dl`   `div`     Element used to wrap the fields           
  `uploader`                       `wp, basic`          `wp`      Whether to use the WP uploader or a basic input for image and file fields  
  `honeypot`                       `checkbox`           `Yes`     Whether to include a hidden input field to capture non human form submission
========================== =========================== ========= ===============================================================================================

.. _auth-settings:
Authorization Settings
----------------------

In this section, you can define who can see and use the form.

.. image:: img/auth-att.jpg

Authorization Attributes
^^^^^^^^^^^^^^^^^^^^^^^^
=============== ============= ========== =============================================================================
  Attribute      Type/Values   Default    Description                               
=============== ============= ========== =============================================================================
  `logged_in`     `checkbox`   `false`    If set to 'Yes' only logged in users will be able to see and use the form                                                                                           Extra HTML to add before the fields       
  `roles`          `array`                Comma separated names of the Wordpress roles that can see and use the form                                                                                  Extra HTML to add after the fields        
  `users`          `array`                Comma separated IDs of Wordpress users that can see and use the form 
=============== ============= ========== =============================================================================

.. _html-settings:
HTML Settings
-------------

.. note:: since the version 2.0, all html attributes are deprecated and replaced by enclosed shortcodes

With these settings related to HTML, you can customize HTML related to the ACF Form, they are all mapped to :code:`acf_form()` function.

HTML Shortcodes
^^^^^^^^^^^^^^^
======================== ============= ============================================================================================ ====================================================================================================
  Shortcode               Type/Values   Default                                                                                      Description                               
======================== ============= ============================================================================================ ====================================================================================================
  `[before_fields]`       `string`                                                                                                  Extra HTML to add before the fields       
  `[after_fields]`        `string`                                                                                                  Extra HTML to add after the fields        
  `[updated_message]`     `string`     `<div id="message" class="updated"><p>%s</p></div>`                                          HTML used to render the updated message   
  `[submit_button]`       `string`     `<input type="submit" class="acf-button button button-primary button-large" value="%s" />`   HTML used to render the submit button     
  `[submit_spinner]`      `string`     `<span class="acf-spinner"></span>`                                                          HTML used to render the submit button loading spinner 
======================== ============= ============================================================================================ ====================================================================================================

Example
^^^^^^^
This shortcode sample will customize the update message and the submit button

.. code:: php
   
   [acf_front_form post_id="new_post"]
      [updated_message]
         <div id="message" class="updated"><span>%s</span></div>
      [/updated_message]
      [submit_button]
         <input type="submit" class="my-class acf-button button button-primary button-small" value="%s" />
      [/submit_button]
   [/acf_front_form]

.. code::

.. _table-settings:
Table Settings
--------------

When the :code:`field_el` is set to :code:`td` or :code:`tr`, the :code:`acf_form()` function will not add the table tags ( `read more <https://www.advancedcustomfields.com/resources/acf_form/#notes>`_ )
ACF Front Form will do the job for you if you want, to do so, Check the * Add the table tags *
.. note:: These attributes are not part of the :code:`acf_form()` function

.. image:: img/table-att.jpg

Shortcode attributes
^^^^^^^^^^^^^^^^^^^^
============ ============= ========= =================================================================================================================================================
 Attribute    Type/Values   Default   Description                               
============ ============= ========= =================================================================================================================================================
 `table_el`   `checkbox`              Whether or not to add table tags before and after the fields, `field_el` should be set to `td` or `tr`  
 `thead`       `string`               Comma separated text added into `td` tags of the `thead` tag if the `table_el` is checked, to use default field labels,set this attribute to '@'  
 `tfoot`       `string`               Comma separated text added into `td` tags of the `tfoot` tag if the `table_el` is checked, to use default field labels,set this attribute to '@'  
============ ============= ========= =================================================================================================================================================
