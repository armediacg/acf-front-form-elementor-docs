What's New in Version 2.0
=========================

The version 2.0 has beed released in 02-01-2020 whith these new features :

Added Authorizations
^^^^^^^^^^^^^^^^^^^^

This, as it says, allows you to define who can see and use the form, it has 3 types :
    - **Logged in** : wheither or not to display the form for guesses or only logged in users
    - **Roles** : you can choose which roles has ability to use the form
    - **Users** : whith this option, you can select specific users which can see and use the form

You can see more details at :ref:`Authorization Settings` section.

Added Elementor UI Style Tabs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This new feature was largely asked by many users and we are proud to bring it ! here some of all styles that may be done in Elementor Style Tab :
    - Ability to style Form, Fields, Inputs and Submit Button
    - Ability to style Wordpress contant buttons
    - Ability to set Typography, Text Color and Text shadow
    - Ability to set Margin, Padding, Box Shadow and Borders for all elements (or most of them)
    - Slider for Label width
    - Custom style for Normal and Hover with transition duration
    - Add new post swith option

Due to complexity of all possible customizations, we provided only the must have features, and more style settings are coming in the future.

ACF Labels in Table header/footer
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Ability to use default ACF label fields and instructions in table header and/or footer when you set field element in :ref:`Fields Settings` to :code:`td` (table data). see more details in :ref:`Table Settings` section.

New Enclosed shortcodes for further releases
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Some of the old attributes of the shortcode causes some issues, so we have moved them outside the main shortcode and made them enclosed shortcode.
In this way, more advanced settings may be used whithout causing any conflicts, you can see those new shortcodes in :ref:`HTML Settings` section.
These new shortcodes are not implemented in Elementor UI for the moment and will be included in next update.

Finally, some other fixes and code optimizations was done. We hope that whith this new release all our users of the plugin will find a better way to achieve their goal.
Many other features are coming soon :)